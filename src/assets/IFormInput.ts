export enum IFormEnum {
  email = "email",
  password = "password",
  repeatPassword = "repeatPassword",
}

export interface IFormInput {
  email: IFormEnum;
  password: IFormEnum;
  repeatPassword: IFormEnum;
}
