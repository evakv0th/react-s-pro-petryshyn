import { useState } from "react";

const usePasswordToggle = (initialVisibility = false) => {
  const [passwordShown, setPasswordShown] = useState(initialVisibility);

  const togglePassword = () => {
    setPasswordShown(!passwordShown);
  };

  return { passwordShown, togglePassword };
};

export default usePasswordToggle;