import "./App.scss";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { SignIn } from "./components/pages/signIn/signIn";
import { ForgotPassword } from "./components/pages/forgotPassword/forgotPassword";
import { ResetPassword } from "./components/pages/resetPassword/resetPassword";
import { SignUp } from "./components/pages/signUp/signUp";
import { NavBar } from "./components/NavBar/NavBar";
import { Home } from "./components/pages/Home/Home";

function App() {
  return (
    <>
      <BrowserRouter>
        <NavBar />
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="/sign-in" element={<SignIn />} />
          <Route path="/sign-up" element={<SignUp />} />
          <Route path="/forgot-password" element={<ForgotPassword />} />
          <Route path="/reset-password" element={<ResetPassword />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
