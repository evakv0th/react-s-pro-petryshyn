import { IFormEnum } from "../../assets/IFormInput";
import "./Input.scss";

type InputProps = {
  type: string;
  minLength?: number;
  textAbove?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  value?: string;
  placeholder: string;
  name: IFormEnum;
  register: any;
};

export function Input({
  type,
  minLength,
  textAbove,
  value,
  onChange,
  placeholder,
  name,
  register,
}: InputProps) {
  return (
    <div className="input-container">
      {textAbove}
      <input
        type={type}
        minLength={minLength}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        {...register(name)}
        required
      />
    </div>
  );
}
