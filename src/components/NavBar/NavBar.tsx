import "./NavBar.scss";
import { Link } from "react-router-dom";

export function NavBar() {
  return (
    <nav className="NavBar">
      <Link to={"/"}>
        <h3>Home</h3>
      </Link>
      <Link to={"/sign-in"}>
        <h3>Sign in</h3>
      </Link>
      <Link to={"/sign-up"}>
        <h3>Sign up</h3>
      </Link>
      <Link to={"/forgot-password"}>
        <h3>Forgot password</h3>
      </Link>
      <Link to={"/reset-password"}>
        <h3>Reset password</h3>
      </Link>
    </nav>
  );
}
