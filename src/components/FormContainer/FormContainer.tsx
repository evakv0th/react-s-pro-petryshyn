import "./FormContainer.scss";

type InputProps = {
  image: string;
  titleText: string;
  altText: string;
  children: any;
  onSubmit?: any;
};
export function FormContainer({
  children,
  image,
  titleText,
  altText,
  onSubmit,
}: InputProps) {
  return (
    <div className="container">
      <img src={image} alt={altText} />
      <form onSubmit={onSubmit}>
        <h3>{titleText}</h3>
        {children}
      </form>
    </div>
  );
}
