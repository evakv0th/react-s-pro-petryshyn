
import "./Button.scss";

type InputValues = {
  disabled?: boolean;
  buttonText: string;
  style?: any;
  onClick?: any;
};

export function Button({ disabled, buttonText, style, onClick }: InputValues) {
  return (
    <button name="button" type="submit" disabled={disabled} style={style} onClick={onClick}>
      {buttonText}
    </button>
  );
}
