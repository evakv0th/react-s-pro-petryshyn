import "./Checkbox.scss";

type InputProps = {
  togglePassword: () => void;
};
export function Checkbox({ togglePassword }: InputProps) {
  return (
    <div className="checkbox-container">
      <input type="checkbox" id="togglePass" onClick={togglePassword} />
      <label htmlFor="togglePass">Show Password</label>
    </div>
  );
}
