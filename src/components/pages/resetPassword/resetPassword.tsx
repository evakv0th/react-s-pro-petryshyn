import { useNavigate } from "react-router-dom";
import { Button } from "../../Button/Button";
import { FormContainer } from "../../FormContainer/FormContainer";
import { Input } from "../../Input/Input";
import "../pages.scss";
import logoImage from "../../../images/Group.svg";
import { Checkbox } from "../../Checkbox/Checkbox";
import { useState } from "react";
import { IFormEnum, IFormInput } from "../../../assets/IFormInput";
import { SubmitHandler, useForm } from "react-hook-form";
import usePasswordToggle from "../../../assets/usePasswordToggle";

export function ResetPassword() {
  const navigate = useNavigate();
  const [resetPressed, setResetPressed] = useState(false);
  const { passwordShown, togglePassword } = usePasswordToggle();
  const { register, handleSubmit, watch, setError } = useForm<IFormInput>();

  const password = watch(IFormEnum.password);
  const confirmPassword = watch(IFormEnum.repeatPassword);

  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    if (validateForm()) {
      console.log(data);
      setResetPressed(true);
    } else {
      setError(IFormEnum.repeatPassword, {
        type: "manual",
        message: "Passwords do not match",
      });
    }
  };
  const validateForm = () => password === confirmPassword;

  return (
    <div className="pageContainer">
      <FormContainer
        image={logoImage}
        titleText={!resetPressed ? "Reset Your Password" : "Password Changed"}
        altText="logo"
        onSubmit={handleSubmit(onSubmit)}
      >
        {!resetPressed ? (
          <>
            <Input
              type={!passwordShown ? "password" : "text"}
              minLength={8}
              textAbove="Password"
              placeholder="password"
              name={IFormEnum.password}
              register={register}
            />
            <Input
              type={!passwordShown ? "password" : "text"}
              minLength={8}
              textAbove="Confirm Password"
              placeholder="confirm password"
              name={IFormEnum.repeatPassword}
              register={register}
            />
            <Checkbox togglePassword={togglePassword} />
            <Button buttonText="Reset" />
            <p className="errorMsg" style={{ opacity: validateForm() ? 0 : 1 }}>
              Passwords do not match!
            </p>
          </>
        ) : (
          <>
            <p className="resetText centeredText">
              You can use your new password to log into your account
            </p>
            <Button
              buttonText="Log In"
              onClick={() => navigate("/sign-in")}
              style={{ marginBottom: "4px" }}
            />
            <Button
              buttonText="Go to Home"
              style={{ backgroundColor: "#fff", color: "#2C343A" }}
              disabled={false}
              onClick={() => navigate("/")}
            />
          </>
        )}
      </FormContainer>
    </div>
  );
}
