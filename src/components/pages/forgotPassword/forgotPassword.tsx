import { Button } from "../../Button/Button";
import { FormContainer } from "../../FormContainer/FormContainer";
import { Input } from "../../Input/Input";
import logoImage from "../../../images/Group.svg";
import "../pages.scss";
import { useNavigate } from "react-router-dom";
import { IFormEnum, IFormInput } from "../../../assets/IFormInput";
import { SubmitHandler, useForm } from "react-hook-form";

export function ForgotPassword() {
  const { register, handleSubmit } = useForm<IFormInput>();
  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    console.log(data);
    navigate("/reset-password");
  };

  const navigate = useNavigate();
  const goBack = () => {
    navigate("/sign-in");
  };

  return (
    <div className="pageContainer">
      <FormContainer
        image={logoImage}
        titleText="Reset Password"
        altText="logo"
        onSubmit={handleSubmit(onSubmit)}
      >
        <p className="resetText">
          Don't worry, happens to the best of us. Enter the email address
          associated with your account and we'll send you a link to reset.
        </p>
        <Input type="email" textAbove="Email" placeholder={"name@mail.com"} register={register} name={IFormEnum.email} />
        <Button buttonText="Reset" style={{ marginBottom: "8px" }} />
        <Button
          buttonText="Cancel"
          style={{ backgroundColor: "#fff", color: "#2C343A" }}
          disabled={false}
          onClick={goBack}
        />
      </FormContainer>
    </div>
  );
}
