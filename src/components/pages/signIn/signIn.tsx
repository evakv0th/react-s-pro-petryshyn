import "../pages.scss";
import logoImage from "../../../images/Group.svg";

import { Button } from "../../Button/Button";
import { Input } from "../../Input/Input";
import { Checkbox } from "../../Checkbox/Checkbox";
import { FormContainer } from "../../FormContainer/FormContainer";
import { useNavigate } from "react-router-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import { IFormInput, IFormEnum } from "../../../assets/IFormInput";
import usePasswordToggle from "../../../assets/usePasswordToggle";

export function SignIn() {
  const navigate = useNavigate();
  const { passwordShown, togglePassword } = usePasswordToggle();
  const { register, handleSubmit } = useForm<IFormInput>();

  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    console.log(data);
    navigate("/");
  };
  return (
    <div className="pageContainer">
      <FormContainer
        image={logoImage}
        titleText="Welcome!"
        altText="logo"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Input
          type="email"
          textAbove="Email"
          placeholder={"name@mail.com"}
          register={register}
          name={IFormEnum.email}
        />
        <Input
          type={!passwordShown ? "password" : "text"}
          minLength={8}
          textAbove="Password"
          placeholder={"password"}
          register={register}
          name={IFormEnum.password}
        />
        <Checkbox togglePassword={togglePassword} />
        <Button buttonText="Login" />
      </FormContainer>
    </div>
  );
}
