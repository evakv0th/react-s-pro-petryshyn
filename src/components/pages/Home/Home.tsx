import "../pages.scss";
import logoImage from "../../../images/Group.svg";
import { FormContainer } from "../../FormContainer/FormContainer";

export function Home() {
  return (
    <div className="pageContainer">
      <FormContainer image={logoImage} titleText="Home Page!" altText="logo">
        <p>Hello World</p>
      </FormContainer>
    </div>
  );
}
