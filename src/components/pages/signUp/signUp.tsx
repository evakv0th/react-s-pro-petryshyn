import "../pages.scss";
import logoImage from "../../../images/Group.svg";
import { Button } from "../../Button/Button";
import { Input } from "../../Input/Input";
import { Checkbox } from "../../Checkbox/Checkbox";
import { FormContainer } from "../../FormContainer/FormContainer";
import { IFormEnum, IFormInput } from "../../../assets/IFormInput";
import { useForm, SubmitHandler } from "react-hook-form";
import usePasswordToggle from "../../../assets/usePasswordToggle";

export function SignUp() {
  const { passwordShown, togglePassword } = usePasswordToggle();
  const { register, handleSubmit, watch, setError } = useForm<IFormInput>();

  const password = watch(IFormEnum.password);
  const confirmPassword = watch(IFormEnum.repeatPassword);

  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    if (!validateForm()) {
      setError(IFormEnum.repeatPassword, {
        type: "manual",
        message: "Passwords do not match",
      });
      return;
    }
    console.log(data);
  };

  const validateForm = () => password === confirmPassword;

  return (
    <div className="pageContainer">
      <FormContainer
        image={logoImage}
        titleText="Register your account"
        altText="logo"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Input
          type="email"
          textAbove="Email"
          placeholder="name@mail.com"
          name={IFormEnum.email}
          register={register}
        />
        <Input
          type={!passwordShown ? "password" : "text"}
          minLength={8}
          textAbove="Password"
          placeholder="password"
          name={IFormEnum.password}
          register={register}
        />
        <Input
          type={!passwordShown ? "password" : "text"}
          minLength={8}
          textAbove="Confirm Password"
          placeholder="confirm password"
          name={IFormEnum.repeatPassword}
          register={register}
        />
        <Checkbox togglePassword={togglePassword} />
        <Button buttonText="Register" />
        <p className="errorMsg" style={{opacity: validateForm() ? 0 : 1}}>Passwords do not match!</p>
      </FormContainer>
    </div>
  );
}
